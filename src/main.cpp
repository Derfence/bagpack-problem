#include <iostream>
#include <omp.h>
#include <time.h>
#include <omp.h>
#include "Solveur.hpp"
#include "Bagpack.hpp"

int main(int argc, char **argv)
{
    unsigned verbose = 0;
    if (argc <= 4)
        return 1;
    srand(time(NULL));

    //Fonctions init
    auto capaciteFct = [&argv](Container &it) { it.capacite = atoi(argv[3]); };
    auto demandeFct = [](Demandeur &it) { it.demande = (unsigned)(rand() * 100.0 / RAND_MAX); };

    double validPourcent = 0;
    unsigned nbTests = atoi(argv[4]);
#pragma omp parallel for
    for (unsigned testId = 0; testId < nbTests; testId++)
    {
        Bagpack bp(atoi(argv[1]), demandeFct, atoi(argv[2]), capaciteFct, solveurGlouton);

        if (verbose == 2)
        {
            for_each(bp.containers, bp.containers + atoi(argv[2]), [](Container &it) { cout << it << endl; });
            for_each(bp.demandeurs, bp.demandeurs + atoi(argv[1]), [](Demandeur &it) { cout << it << endl; });
            cout << endl;
        }
        else
        {
            unsigned unattributed = 0;
            for_each(bp.demandeurs, bp.demandeurs + atoi(argv[1]), [&unattributed](Demandeur &it) {
                unattributed += it.attribution >= numeric_limits<unsigned>::max() - 1; // non attribués
            });
            if (verbose == 1)
            {
                cout << unattributed << " demandeurs non attribués (" << unattributed * 100.0 / atoi(argv[1]) << "%)";
                cout << endl;
            }
            validPourcent += (atoi(argv[1]) - unattributed) * 100.0 / atoi(argv[1]);
        }
    }
    cout << validPourcent / nbTests << "% demandeurs attribués" << endl;

    return 0;
}