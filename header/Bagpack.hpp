#ifndef BAGPACK_HPP
#define BAGPACK_HPP

#include <vector>
#include <algorithm>
#include "Container.hpp"
#include "Demandeur.hpp"

using namespace std;

class Bagpack
{
public:
    Demandeur *demandeurs;
    Container *containers;

    template <typename fct1, typename fct2, typename Solveur>
    Bagpack(unsigned nbDemandeur, fct1 demandeFct, unsigned nbContainer, fct2 capaciteFct, Solveur solveur) : demandeurs(new Demandeur[nbDemandeur]), containers(new Container[nbContainer])
    {
        for_each(containers, containers + nbContainer, capaciteFct);
        for (unsigned i = 0; i < nbContainer; i++)
            containers[i].pos = i;
        for_each(demandeurs, demandeurs + nbDemandeur, demandeFct);
        for (unsigned i = 0; i < nbDemandeur; i++)
        {
            double pos = i < 150 ? rand() * nbContainer * 1.0 / RAND_MAX : demandeurs[(unsigned)(rand() * 150.0 / RAND_MAX)].pos;
            demandeurs[i].pos = pos;
            demandeurs[i].Init(containers, nbContainer);
        }

        solveur(demandeurs, nbDemandeur, containers, nbContainer);
    }
    Bagpack(const Bagpack &bp) : demandeurs(bp.demandeurs), containers(bp.containers) {}
    const Bagpack &operator=(const Bagpack &) { return *this; }
    ~Bagpack()
    {
        delete[] containers;
        delete[] demandeurs;
    }
};

#endif