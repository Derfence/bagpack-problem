#ifndef CONTAINEUR_HPP
#define CONTAINEUR_HPP

class Container
{
public:
    unsigned pos;
    double capacite;
    Container(double capacite) : pos(0), capacite(capacite) {}
    Container() : pos(0), capacite(0) {}
    ~Container() {}
};

template <typename ostream>
ostream &operator<<(ostream &os, const Container &d)
{
    os << "pos:" << d.pos << " capacite:" << d.capacite;
    return os;
}

#endif