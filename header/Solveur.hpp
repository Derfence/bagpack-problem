#ifndef SOLVEUR_HPP
#define SOLVEUR_HPP

#include <vector>
#include <algorithm>
#include "Container.hpp"
#include "Demandeur.hpp"

using namespace std;

auto solveurGlouton = [](Demandeur *demandeurs, unsigned nbDemandeur, Container *containers, unsigned nbContaineur) {
    //copier et trier la liste des demandes
    unsigned *demandesTriees = new unsigned[nbDemandeur];
    unsigned *capacites = new unsigned[nbContaineur];
    for_each(capacites, capacites + nbContaineur, [&containers](unsigned &it) { it = containers[0].capacite; });
    for (unsigned i = 0; i < nbDemandeur; i++)
        demandesTriees[i] = demandeurs[i].demande;
    sort(demandesTriees, demandesTriees + nbDemandeur, greater<>());

    //Attribuer les demandes par ordres décroissantes
    for (unsigned i = 0; i < nbDemandeur; i++)
    {
        //Indice de la prochaine demande à attribuer
        unsigned j = 0;
        while (demandeurs[j].demande != demandesTriees[i] || demandeurs[j].attribution != numeric_limits<unsigned>::max())
            j++;
        //Trouver le premier containeur qui peut l'accepter
        unsigned attribution = 0;
        while (attribution < 10 && capacites[demandeurs[j].containerVisible[attribution].pos] < demandeurs[j].demande)
            attribution++;
        //Si trouvé, attibuer
        if (attribution < 10)
        {
            demandeurs[j].attribution = attribution;
            capacites[demandeurs[j].containerVisible[attribution].pos] -= demandeurs[j].demande;
        }
        else
            demandeurs[j].attribution--; //!=numeric_limits<unsigned>::max() mais non attribué
    }

    delete[] demandesTriees;
    delete[] capacites;
};

#endif