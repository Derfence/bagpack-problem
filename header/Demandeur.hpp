#ifndef DEMANDEUR_HPP
#define DEMANDEUR_HPP

#include <vector>
#include <iostream>
#include <limits>
#include <algorithm>
#include "Container.hpp"

using namespace std;

class Demandeur
{
public:
    double pos;
    double demande;
    Container *containerVisible;
    unsigned attribution;

    Demandeur() : pos(0), demande(0), containerVisible(new Container[10]), attribution(numeric_limits<unsigned>::max()) {}
    Demandeur(double pos, double demande, Container *containerVisible) : pos(pos), demande(demande), containerVisible(containerVisible), attribution(numeric_limits<unsigned>::max()) {}
    Demandeur(const Demandeur &d) : pos(d.pos), demande(d.demande), containerVisible(d.containerVisible), attribution(d.attribution) {}
    void Init(Container *containers, unsigned nbContainer)
    {
        if (pos < 6)
            for (unsigned i = 0; i < 10; i++)
                containerVisible[i] = containers[i];
        else if (pos > nbContainer - 6)
            for (unsigned i = 0; i < 10; i++)
                containerVisible[i] = containers[nbContainer - i];
        else
            for (unsigned i = 0; i < 5; i++)
            {
                containerVisible[5 + i] = containers[(unsigned)pos + i];
                containerVisible[4 - i] = containers[(unsigned)pos - i];
            }
    }
    const Demandeur &operator=(const Demandeur &) { return *this; }
    ~Demandeur()
    {
        delete[] containerVisible;
    }
};

template <typename ostream>
ostream &operator<<(ostream &os, const Demandeur &d)
{
    os << "pos:" << d.pos << " demande:" << d.demande << " containers visibles:";
    for_each(d.containerVisible, d.containerVisible + 10, [&os](Container &it) { os << it.pos << " "; });
    os << " attribué à:" << d.attribution;
    return os;
}

#endif